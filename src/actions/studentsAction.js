import axios from "axios";

export const GET_STUDENTS_LIST = "GET_STUDENTS_LIST";
export const POST_USER_CREATE = "POST_USER_CREATE";

export const getStudentsList = () => {
  return (dispatch) => {
    axios
      .get("http://127.0.0.1:8000/api/student/")
      .then(function (response) {
        dispatch ({
            type: GET_STUDENTS_LIST,
            payload: {
                data: response.data.data,
                errorMessage: false
            }
        })
      })
      .catch(function (error) {
        dispatch ({
            type: GET_STUDENTS_LIST,
            payload: {
                data: false,
                errorMessage: error.message,
            }
        })
      })
  };
};

export const postUserCreate = (data) => {
  return (dispatch) => {
    axios
      .post(
         "http://127.0.0.1:8000/api/student/create",
        data
      )
      .then(function (response) {
        console.log(response);
        
        // dispatch({
        //   type: POST_USER_CREATE,
        //   payload: {
        //     data: response.data,
        //     errorMessage: false,
        //   },
        // });
      })
      .catch(function (error) {
        // dispatch({
        //   type: POST_USER_CREATE,
        //   payload: {
        //     data: false,
        //     errorMessage: error.message,
        //   },
        // });
      });
  };
};
