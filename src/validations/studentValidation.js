const studentValidation = (values) => {

    const errors = {};

    if (!values.name || values.name === "") {
      errors.name = "Nama harus diisi";
    }
  
    if (!values.no_tlp || values.no_tlp === "") {
      errors.no_tlp = "No Tlp harus diisi";
    }
  
    if (!values.email || values.email === "") {
      errors.email = "Email harus diisi";
    }
  
    if (!values.alamat || values.alamat === "") {
      errors.alamat = "Alamat harus diisi";
    }
  
    return errors
    
}

export default studentValidation
