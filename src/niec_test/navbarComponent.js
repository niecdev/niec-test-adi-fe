import React from 'react'
import { Nav, Navbar, Form, FormControl, Button } from 'react-bootstrap'

export default function navbarComponent() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#">NIEC TEST</Navbar.Brand>
            <Navbar.Collapse id="navbarScroll">
                <Nav
                    className="mr-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                >
                <Nav.Link href="#action1">Student</Nav.Link>
                </Nav>
                <Navbar.Collapse className="justify-content-end">
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar.Collapse>
        </Navbar>
        
    )
}

