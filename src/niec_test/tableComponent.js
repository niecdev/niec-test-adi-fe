import Button from '@restart/ui/esm/Button';
import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect } from 'react-redux';

const { SearchBar } = Search;
const columns = [{
    dataField: 'id',
    text: 'ID',
    sort: true
  }, {
    dataField: 'name',
    text: 'Nama',
    sort: true
  }, {
    dataField: 'no_tlp',
    text: 'No Telp / WA',
  }, {
    dataField: 'email',
    text: 'Email',
  }, {
    dataField: 'alamat',
    text: 'Alamat',
  }];

const defaultSorted = [{
  dataField: 'id',
  order: 'asc'
}];

const MapStateToProps = (state) => {
  return {
    getStudentsList: state.students.getStudentsList,
    errorStudentList: state.students.errorStudentList
  }
}

const tableComponent = (props) => {
    return (
        <div className="mt-5">
            { props.getStudentsList ? 
                <ToolkitProvider 
                    bootstrap4 
                    keyField='id' 
                    data={ props.getStudentsList } 
                    columns={ columns } 
                    defaultSorted={ defaultSorted }  
                    search>
                    {
                        props => (
                            <div>
                                <SearchBar { ...props.searchProps } />
                                <hr />
                                <BootstrapTable { ...props.baseProps } pagination={ paginationFactory() }/>
                            </div>
                        )
                    }
                </ToolkitProvider> 
                : null 
            }
        </div>
    )
}

export default connect(MapStateToProps, null) (tableComponent)
