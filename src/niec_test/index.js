import React, { Component } from 'react';
import FormComponent from './formComponent';
import NavbarComponent from './navbarComponent';
import TableComponent from './tableComponent';
import { connect } from 'react-redux';
import { getStudentsList } from '../actions/studentsAction'; 
import { postUserCreate } from '../actions/studentsAction'; 

class index extends Component {
    componentDidMount() {
        this.props.dispatch(getStudentsList());
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    handleSubmit = (data) => {
        console.log(data);
        this.props.dispatch(postUserCreate(data));
        window.location.reload();
    }

    render() {
        return (
            <div>
                <NavbarComponent />
                <div className="container mt-4">
                    {/* <FormComponent {...this.state} handleChange={this.handleChange} handleSubmit={this.handleSubmit} /> */}
                    <FormComponent onSubmit={(data) => this.handleSubmit(data)}/>
                    <TableComponent />
                </div>
            </div>
        )
    }
}

export default connect() (index)

