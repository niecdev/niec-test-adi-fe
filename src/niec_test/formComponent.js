import React, { Component } from 'react';
import { reduxForm, Field } from "redux-form";
import { FormGroup, Col, label, input, Row, Button } from "react-bootstrap";
import { connect } from "react-redux";
import studentValidation from '../validations/studentValidation';

const renderField = ({
    input,
    type,
    placeholder,
    label,
    disabled,
    readOnly,
    meta: { touched, error, warning },
  }) => (
    <Row>
      <Col md="12">
        <label htmlFor="{input}" className="col-form-label">
          {label}
        </label>
      </Col>
      <Col md="12">
        <input
          {...input}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          readOnly={readOnly}
        ></input>
        {touched &&
          ((error && <p style={{ color: "red" }}>{error}</p>) ||
            (warning && <p style={{ color: "brown" }}>{warning}</p>))}
      </Col>
    </Row>
);

const mapStateToProps = (state) => {
    return {
        initialValues : {
            nama : state.users.getUserDetail.nama,
            nohp : state.users.getUserDetail.nohp,
            alamat : state.users.getUserDetail.alamat,
            umur : state.users.getUserDetail.umur,
        }
    };
};

class formComponent extends Component {
    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
                <FormGroup row>
                    <Col md={12}>
                        <FormGroup>
                        <Field
                            type="text"
                            name="name"
                            component={renderField}
                            label="Nama :"
                        />
                        </FormGroup>
                    </Col>

                    <Col md={12}>
                        <FormGroup>
                        <Field
                            type="text"
                            name="no_tlp"
                            component={renderField}
                            label="No. Telp / WA :"
                        />
                        </FormGroup>
                    </Col>

                    <Col md={12}>
                        <FormGroup>
                        <Field
                            type="text"
                            name="email"
                            component={renderField}
                            label="Email :"
                        />
                        </FormGroup>
                    </Col>

                    <Col md={12}>
                        <FormGroup>
                        <Field
                            type="textarea"
                            row="3"
                            name="alamat"
                            component={renderField}
                            label="Alamat :"
                        />
                        </FormGroup>
                    </Col>
                </FormGroup>

                <FormGroup row className="mt-5">
                    <Col md="12">
                        <FormGroup>
                        <Button
                            color="dark"
                            type="submit"
                            disabled={this.props.submitting}
                        >
                            Submit
                        </Button>
                        </FormGroup>
                    </Col>
                </FormGroup>
            </form>
        )
    }
}

formComponent = reduxForm({
    form: "formCreateStudent",
    validate: studentValidation,
    enableReinitialize: true
})(formComponent);


export default connect() (formComponent)
