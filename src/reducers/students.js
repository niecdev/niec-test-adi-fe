import { GET_STUDENTS_LIST } from "../actions/studentsAction";
import { POST_USER_CREATE } from "../actions/studentsAction";

let initialState = {
    getStudentsList: false,
    getResponseDataUser: false,
    errorStudentList: false,
    errorResponseDataUser: false,
}

const students = (state = initialState, action) => {
    switch (action.type) {
        case GET_STUDENTS_LIST:
            return {
                ...state,
                getStudentsList: action.payload.data,
                errorStudentList: action.payload.errorMessage
            }
        break;

        case POST_USER_CREATE:
            return {
                ...state,
                getResponseDataUser: action.payload.data,
                errorResponseDataUser: action.payload.errorMessage
            }
        break;
    
        default:
            return state;
    }
}

export default students
